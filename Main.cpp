#include <iostream>
#include <string>

int main()
{
	system("color F4");

	std::cout << "Enter your favorite fruit: ";
	std::string fruit;
	std::cin >> fruit;

	std::cout << "Your favorite fruit is: " << fruit << "\n";
	std::cout << "Number of symbols: " << fruit.length() << "\n";
	std::cout << "First symbol is: " << * fruit.begin() << "\n";
	std::cout << "Last symbol is: " << * fruit.rbegin() << "\n";

	return 0;
}